library(RColorBrewer)
library(progress)
library(drc)



source("config.R")

#### settings and import ####

# color settings
lgs = c("eng", "spa", "ger", "ita")
colors = brewer.pal(length(lgs), "PiYG")
names(colors) = lgs

# bootstrapping settings
bootstrap = FALSE    # set to FALSE to load pre-computed estimates 
n_sample = 300

# import estimates
df = readRDS("estimates/estimates_all.rds")


#### local helpers ####

bootstrap_ME_growth_rate = function(word_freqs, prev_fin, n, n_sample){
  # word_freqs ... frequency trajectory (from corpus)
  # prev_fin ..... contemporary prevalence (from survey)
  # n ............ sample size for estimating prev_fin
  # n_sample ..... number of bootstrapped samples
  
  time = TIME
  
  # min max normalize
  word_freqs = (word_freqs - min(word_freqs)) / (max(word_freqs) - min(word_freqs))
  
  # normalize wrt prevalence of final entry in time series
  word_freqs_norm = word_freqs / word_freqs[length(word_freqs)] * prev_fin
  
  
  # bootstrapping trajectories
  bootstrapped_trajectories = matrix(NA, nrow = length(word_freqs_norm), ncol = n_sample)
  
  for(i in 1:length(word_freqs_norm)){
    if(word_freqs_norm[i] != 0){
      bootstrapped_trajectories[i,] = rbinom(n_sample, size = n, prob = word_freqs_norm[i]) / n
    } else {
      bootstrapped_trajectories[i,] = rep(0, n_sample)
    }
  }
  
  # fit logistic models to bootstrapped trajectories
  bootstrapped_rs = c()
  
  for(i in 1:n_sample){
    
    traj = bootstrapped_trajectories[,i]
    
    # fit model to bootstrapped trajectory
    model = drm(traj ~ time, fct = L.3(), type = "continuous")
    rate = -unname(model$coefficients)[1]   # the drc function returns -k!
    
    bootstrapped_rs = c(bootstrapped_rs, rate)
  }
  
  # compute CI and ME from bootstrapped rates
  bootstrapped_ci = quantile(bootstrapped_rs, c(0.025, 0.975))
  ME = unname(diff(bootstrapped_ci)) / 2
  
  return(ME)
}



#### error estimation for prev and aoa ####

ME_R0_equ_all_lgs = list()
ME_R0_aoa_all_lgs = list()

for(lg in lgs){
  subs = subset(df, language == lg)
  ME_R0_equ_all_lgs[[lg]] = 1 / (1 - subs$prev)^2 * subs$ME_prev |> na.omit()
  ME_R0_aoa_all_lgs[[lg]] = sqrt((ME_LE / subs$aoa)^2 + (LIFE_EXPECTATION / subs$aoa^2 * subs$ME_aoa)^2) |> na.omit()
}



#### error estimation for growth ####

df = na.omit(df)
df = subset(df, increasing == 1)

if(bootstrap){
  ME_r_all_lgs = list()
  
  for(lg in rev(lgs)){
    # for each language, go through all words and bootstra trajectories
    # to obtain margin of error of 95% CI
    
    message(lg)
    subs = subset(df, language == lg)
    
    ME_r = c()
    
    pb = progress_bar$new(
      format = " [:bar] :percent eta: :eta",
      total = length(subs$word))
    
    for(w in subs$word){
      pb$tick()
      
      # get frequency trajectory, prevalence, and sample size
      frequencies = read.csv(paste0("preprocessed/frequencies_", lg, ".csv"))
      word_freqs = as.numeric(subset(frequencies, word == w)[1,-1])
      
      word_data = subset(df, word == w & language == lg)
      prev_fin = word_data$prev
      n = word_data$num
      
      # bootstrap ME based on these data
      ME_r = c(ME_r, bootstrap_ME_growth_rate(word_freqs, prev_fin, n, n_sample))
    }
    
    res = data.frame(
      word = subs$word,
      ME_r = ME_r
    )
    ME_r_all_lgs[[lg]] = res
  }
  saveRDS(ME_r_all_lgs, "complementary_analysis/ME_r_all_lgs.rds")
} else {
  ME_r_all_lgs = readRDS("complementary_analysis/ME_r_all_lgs.rds")  
}

# compute error for R0 based on growth for all languages
ME_R0_growth_all_lgs = list()

for(lg in lgs){
  subs = subset(df, language == lg)
  subs = merge(subs, ME_r_all_lgs[[lg]], by = "word")
  subs = subset(subs, increasing == 1)
  
  ME_R0_growth = sqrt(
    ((LIFE_EXPECTATION - subs$aoa) * subs$ME_r)^2 +
      (subs$r * (ME_LE + subs$ME_aoa))^2
  )
  
  ME_R0_growth_all_lgs[[lg]] = ME_R0_growth  
}


#### collect all and visualize ####

errors = list(
  ME_R0_equ_all_lgs[["eng"]], ME_R0_aoa_all_lgs[["eng"]], ME_R0_growth_all_lgs[["eng"]],
  ME_R0_equ_all_lgs[["spa"]], ME_R0_aoa_all_lgs[["spa"]], ME_R0_growth_all_lgs[["spa"]],
  ME_R0_equ_all_lgs[["ger"]], ME_R0_aoa_all_lgs[["ger"]], ME_R0_growth_all_lgs[["ger"]],
  ME_R0_equ_all_lgs[["ita"]], ME_R0_aoa_all_lgs[["ita"]]
)

for(e in errors){
  print(round(summary(e), 2))
}

par(mar = c(7,3,3,3))
vioplot(errors,
        col = c(colors["eng"], colors["eng"], colors["eng"],
                colors["spa"], colors["spa"], colors["spa"],
                colors["ger"], colors["ger"], colors["ger"],
                colors["ita"], colors["ita"]),
        names = c("prevalence", "acquisition", "growth",
                  "prevalence", "acquisition", "growth",
                  "prevalence", "acquisition", "growth",
                  "prevalence", "acquisition"),
        las = 3, ylim = c(0, 42)
)
text(1:11 + 0., unlist(lapply(errors, median)) + 2, 
     labels = round(unlist(lapply(errors, median)), 2),
     cex = 1.2, pos = 4)

