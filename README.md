
# Estimating the basic reproductive ratio of words

This project features data and code to reproduce the analysis in our study "Lexical innovations are rarely passed on during one’s lifetime: epidemiological perspectives on estimating the basic reproductive ratio of words".

Computations were done in R 4.4.1. Required libraries are listed in `requirements.txt`.

The `data` directory contains frequency data for a set of words extracted from Google ngrams in September 2023 and demographic data openly available at https://data.worldbank.org/ and https://data.un.org/. 

The subdirectory `not_public` is supposed to contain data that are not openly available:
* `alonso_et_al_2017.xlsx` was downloaded from https://doi.org/10.3758/s13428-014-0454-2 
* `birchenough_et_al_2017_aoa.xslx` and `birchenough_et_al_2017_aoa.csv`were downloaded from https://doi.org/10.3758/s13428-016-0718-0
* `brysbaert_et_al_2019_prevalence.xlsx` was downloaded from https://doi.org/10.3758/s13428-018-1077-9
* `kuperman_et_al_2012_aoa.xlsx` was downloaded from https://doi.org/10.3758/s13428-012-0210-4
* `montefinese_et_al_2019_aoa_xlsx` was downloaded from https://doi.org/10.3389/fpsyg.2019.00278
* `davies_et_al_2010_coha_freq_english.txt` was downloaded in May 2017 from https://www.english-corpora.org/coha

Data in`not_public` will not be made available in any public repository.

The `preprocessed` directory contains preprocessed frequency trajectories for all languages based on Google ngrams data and COHA, as described in section 2.1. 

The `estimates` directory contains estimates of the basic reproductive ratio for all languages.

Code for preprocessing frequency trajectories in `preprocessed` and deriving estimates in `estimates` as described in section 2.2.1 in the manuscript can be found in the `preprocessing` directory. 

All estimates are collected together through the script `collect_estimates.R` in a single dataframe saved as `estimates/estimates_all.rds`.

Code for reproducing the analysis as described in section 2.2.2 of the manuscript can be found in the script `analysis.R`. Note that this script will work even when the data in `data/not_public` are not provided. 

The `complementary_analysis` directory contains an R script for reproducing the descriptive demographic analysis in Fig 3 and a Mathematica notebook that was used to analyze the ODE as presented in Fig 1. It also contains code for the analysis of inflection points (Fig 5) and the post-hoc analysis of trajectory similarities in section 4 (both in `robustness_checks.R`) and for the computation of margins of error (section 2.2.2).

Data and code in this project are licensed as `CC BY-NC` https://creativecommons.org/licenses/by-nc/4.0/.


