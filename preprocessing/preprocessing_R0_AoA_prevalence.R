############### R0, AOA AND PREVALENCE (SECTION 2.2.1) ################

library(readxl)
library(pbapply)

source("preprocessing/helpers.R")
source("config.R")

#### ENGLISH ####

#### import and prepare data ####

aoa = read_excel("data/not_public/kuperman_et_al_2012_aoa.xlsx")
prev = read_excel("data/not_public/brysbaert_et_al_2016_prevalence.xlsx")

df = merge(aoa, prev, by = "Word")

# get AoA, frequency, and prevalence
df$aoa = as.numeric(df$Rating.Mean)
df$logfreq = log(df$Freq_pm)
df$prev = df$Pknown

# sample size for prev estimate
df$num = df$Nobs  

# get R0 estimates
df$R0_equ = 1 / (1 - df$Pknown)
df$R0_aoa = LIFE_EXPECTATION / df$aoa

# margin of error
df$ME_prev = sqrt(df$prev * (1 - df$prev) / df$Nobs) * 1.96
df$ME_aoa = as.numeric(df$Rating.SD) / sqrt(as.numeric(df$OccurNum)) * 1.96

# export
df$word = df$Word
df = subset(df, select = c(word, aoa, prev, num, R0_equ, R0_aoa, ME_prev, ME_aoa))

write.csv(df, "estimates/estimates_R0_AoA_prevalence_eng.csv", row.names = FALSE)


#### ITALIAN ####

itaaoa = read_excel("data/not_public/montefinese_et_al_2019_aoa.xlsx")

words = unique(itaaoa$Ita_Word)
aoa = c()
prev = c()
num = c()
sd = c()


for(w in words){
  subs = subset(itaaoa, Ita_Word == w)
  aoa = c(aoa, mean(as.numeric(subs$Rating), na.rm = TRUE))
  n = nrow(subs) - sum(subs$Rating == "Unknown")
  num = c(num, n)
  sd = c(sd, sd(as.numeric(subs$Rating), na.rm = TRUE))
  prev = c(prev, n / nrow(subs))
}

itadf = data.frame(
  word = words, 
  aoa, 
  prev,
  num,
  R0_aoa = LIFE_EXPECTATION / aoa,
  R0_equ = 1 / (1 - prev)
  )

# margin of error
itadf$ME_prev = sqrt(itadf$prev * (1 - itadf$prev) / num) * 1.96
itadf$ME_aoa = sd / sqrt(num) * 1.96      

write.csv(itadf, "estimates/estimates_R0_AoA_prevalence_ita.csv", row.names = FALSE)


#### GERMAN ####

geraoa = read_excel("data/not_public/birchenough_et_al_2017_aoa.xlsx")

num = geraoa$unknown + geraoa$RatperWord

gerdf = data.frame(
  word = geraoa$Word, 
  aoa = geraoa$AoAestimate, 
  prev = geraoa$RatperWord / num,
  num = num
)

gerdf$R0_aoa = LIFE_EXPECTATION / gerdf$aoa
gerdf$R0_equ = 1 / (1 - gerdf$prev)

# margin of error
gerdf$ME_prev = sqrt(gerdf$prev * (1 - gerdf$prev) / num) * 1.96
gerdf$ME_aoa = geraoa$SD / sqrt(geraoa$RatperWord) * 1.96


write.csv(gerdf, "estimates/estimates_R0_AoA_prevalence_ger.csv", row.names = FALSE)


#### SPANISH ####

spaaoa = read_excel("data/not_public/alonso_et_al_2017.xlsx")

spadf = data.frame(
  word = spaaoa$Word, 
  aoa = spaaoa$AoA_average, 
  prev = spaaoa$n / 50,    # 50 ratings per word
  num = 50
)

spadf$R0_aoa = LIFE_EXPECTATION / spadf$aoa
spadf$R0_equ = 1 / (1 - spadf$prev)

# margin of error
spadf$ME_prev = sqrt(spadf$prev * (1 - spadf$prev) / 50) * 1.96
spadf$ME_aoa = spaaoa$AoA_SD / sqrt(spaaoa$n) * 1.96


write.csv(spadf, "estimates/estimates_R0_AoA_prevalence_spa.csv", row.names = FALSE)


